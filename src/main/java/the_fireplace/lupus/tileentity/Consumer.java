package the_fireplace.lupus.tileentity;

import cofh.api.energy.IEnergyReceiver;
import net.minecraft.tileentity.TileEntity;

/**
 * @author The_Fireplace
 */
public abstract class Consumer extends TileEntity implements IEnergyReceiver {

}
