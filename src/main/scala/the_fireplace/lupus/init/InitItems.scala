package the_fireplace.lupus.init

import net.minecraft.client.Minecraft
import net.minecraft.client.resources.model.ModelResourceLocation
import net.minecraft.item.Item
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPreInitializationEvent}
import net.minecraftforge.fml.common.registry.GameRegistry
import net.minecraftforge.fml.relauncher.{Side, SideOnly}
import the_fireplace.lupus.LUPUS

/**
  * @author The_Fireplace
  */
object InitItems {
  val antenna = new Item().setUnlocalizedName("antenna").setCreativeTab(LUPUS.tabLupus)

  def preInit(event:FMLPreInitializationEvent) {
    registerItem(antenna)
  }

  def init(event:FMLInitializationEvent) {
    if(event.getSide.isClient)
      registerRenders()
  }

  def registerRenders(){
    registerRender(antenna)
  }

  def registerItem(item:Item) {
    GameRegistry.registerItem(item, item.getUnlocalizedName.substring(5))
  }
  @SideOnly(Side.CLIENT)
  def registerRender(item:Item) {
    Minecraft.getMinecraft.getRenderItem.getItemModelMesher.register(item, 0, new ModelResourceLocation(LUPUS.MODID + ":"+item.getUnlocalizedName.substring(5), "inventory"))
  }
}
