package the_fireplace.lupus.init

import net.minecraft.block.Block
import net.minecraft.client.Minecraft
import net.minecraft.client.resources.model.ModelResourceLocation
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPreInitializationEvent}
import net.minecraftforge.fml.common.registry.GameRegistry
import net.minecraftforge.fml.relauncher.{SideOnly, Side}
import the_fireplace.lupus.LUPUS
import the_fireplace.lupus.block.BlockMachineFrame

/**
  * @author The_Fireplace
  */
object InitBlocks {
  val machine_frame = new BlockMachineFrame

  def preInit(event:FMLPreInitializationEvent) {
    registerBlock(machine_frame)
  }

  def init(event:FMLInitializationEvent) {
    if(event.getSide.isClient)
      registerRenders()
  }

  def registerRenders(){
    registerRender(machine_frame)
  }

  def registerBlock(block:Block) {
    GameRegistry.registerBlock(block, block.getUnlocalizedName.substring(5))
  }
  @SideOnly(Side.CLIENT)
  def registerRender(block:Block) {
    Minecraft.getMinecraft.getRenderItem.getItemModelMesher.register(net.minecraft.item.Item.getItemFromBlock(block), 0, new ModelResourceLocation(LUPUS.MODID + ":"+block.getUnlocalizedName.substring(5), "inventory"))
  }
}
