package the_fireplace.lupus.block

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import the_fireplace.lupus.LUPUS

/**
  * @author The_Fireplace
  */
class BlockMachineFrame extends Block(Material.iron) {
  setHardness(4.0F)
  setUnlocalizedName("machine_frame")
  setCreativeTab(LUPUS.tabLupus)
  setHarvestLevel("pickaxe", 1)
}
