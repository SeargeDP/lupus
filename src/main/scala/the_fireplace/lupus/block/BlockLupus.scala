package the_fireplace.lupus.block

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import the_fireplace.lupus.LUPUS

/**
  * @author The_Fireplace
  */
abstract class BlockLupus(mat:Material) extends Block(mat:Material) {
  setCreativeTab(LUPUS.tabLupus)
  setHardness(4.0F)
  setHarvestLevel("pickaxe", 1)
}
