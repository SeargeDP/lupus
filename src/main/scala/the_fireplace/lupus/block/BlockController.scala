package the_fireplace.lupus.block

import net.minecraft.block.material.Material
import net.minecraft.block.state.IBlockState
import net.minecraft.tileentity.TileEntity
import net.minecraft.world.World
import the_fireplace.lupus.tileentity.TileEntityController

/**
  * @author The_Fireplace
  */
class BlockController extends BlockLupus(Material.iron) {

  setUnlocalizedName("lupus_controller")

  override def createTileEntity(world: World, state: IBlockState): TileEntity = new TileEntityController()

  override def hasTileEntity(state: IBlockState): Boolean = true
}
