package the_fireplace.lupus.tileentity

import com.google.common.collect.Maps
import net.minecraft.block.Block
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.EnumFacing

/**
  * @author The_Fireplace
  */
class TileEntityController extends Consumer {

  var connections = Maps.newHashMap()[EnumFacing, Block]

  override def receiveEnergy(from: EnumFacing, maxReceive: Int, simulate: Boolean): Int ={

    0
  }

  override def getEnergyStored(from: EnumFacing): Int = 0

  override def getMaxEnergyStored(from: EnumFacing): Int = 0

  override def canConnectEnergy(from: EnumFacing): Boolean = true

  override def writeToNBT(compound: NBTTagCompound){

  }

  override def readFromNBT(compound: NBTTagCompound){

  }

  def connectedTo(block:Block) {
    if(this.worldObj.getBlockState(this.pos.up()) == block.getDefaultState || this.worldObj.getBlockState(this.pos.down()) == block.getDefaultState || this.worldObj.getBlockState(this.pos.north()) == block.getDefaultState || this.worldObj.getBlockState(this.pos.south()) == block.getDefaultState || this.worldObj.getBlockState(this.pos.east()) == block.getDefaultState || this.worldObj.getBlockState(this.pos.west()) == block.getDefaultState)
      true
    false
  }
}
