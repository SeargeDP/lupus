package the_fireplace.lupus

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.Item
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPreInitializationEvent}
import the_fireplace.lupus.init._

/**
  * @author The_Fireplace
  */
@Mod(modid=LUPUS.MODID, name=LUPUS.MODNAME, modLanguage = "scala")
object LUPUS {
  final val MODID = "lupus"

  final val MODNAME = "LUnar Planetary Ultrastrike System"

  var VERSION = ""

  final val curseCode = ""//TODO: Curse Code

  val tabLupus:CreativeTabs = new CreativeTabs("tabLupus") {
    override def getTabIconItem: Item = Item.getItemFromBlock(InitBlocks.machine_frame)
  }

  @EventHandler
  def preInit(event: FMLPreInitializationEvent) {
    InitBlocks.preInit(event)
    InitItems.preInit(event)
  }

  @EventHandler
  def load(event: FMLInitializationEvent) {
    InitBlocks.init(event)
    InitItems.init(event)
  }
}
