package the_fireplace.lupus.config

/**
  * @author The_Fireplace
  */
object ConfigValues {
  val USEPOWER_DEFAULT = true
  var USEPOWER:Boolean = _
  val USEPOWER_NAME = "cfg.usepower"
}
